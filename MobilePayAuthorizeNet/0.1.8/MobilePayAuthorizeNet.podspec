Pod::Spec.new do |s|
    s.name         = "MobilePayAuthorizeNet"
    s.version      = "0.1.8"
    s.summary      = "A brief description of MobilePayAuthorizeNet project."
    s.description  = <<-DESC
    An extended description of MobilePayAuthorizeNet project.
    DESC
    s.homepage     = "http://justtappit.com"
    s.license = { :type => 'Copyright', :text => <<-LICENSE
                   Copyright 2018
                   Permission is granted to...
                  LICENSE
                }
    s.author             = { "$(git config user.name)" => "$(git config user.email)" }
    s.source       = { :git => "https://gitlab.com/tappitmobilepay/iOSMobilePayAuthorizeNetBinary.git", :tag => "#{s.version}" }
    s.vendored_frameworks = "MobilePayAuthorizeNet.xcframework"
    s.platform = :ios
    s.dependency 'AuthorizeNetAccept', '~> 0.5.0'
    s.swift_version = "5.2"
    s.ios.deployment_target  = '11.4'
end